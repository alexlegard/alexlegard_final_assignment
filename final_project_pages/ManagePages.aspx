﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManagePages.aspx.cs" Inherits="final_project_pages.ManagePages" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manage pages</title>
    <link href="~/Style/Style.css" rel="stylesheet" type="text/css" media="screen" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <!--LINK TO CREATE A NEW PAGE-->
            <a id="create_link" href="AddNewPage.aspx">Add Page</a>

            <h1>Manage pages</h1>
            <!--Need to create an SQLDataSource-->
            <asp:SqlDataSource runat="server" id="pages_select" ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
            </asp:SqlDataSource>

            <div id="queryString" class="querybox" runat="server">
                <!--Output the SQL Query to this output box-->
            </div>
            <asp:DataGrid id="page_list" runat="server">
            </asp:DataGrid>
            <div id="debug" runat="server">
            </div>
            
        </div>
    </form>
</body>
</html>