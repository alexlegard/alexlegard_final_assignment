﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pages.aspx.cs" Inherits="final_project_pages.Pages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <a href="">Add a new page</a>
    <asp:SqlDataSource runat="server" SelectCommand="select * from Pages" ID="page_select" ConnectionString="<%$ConnectionStrings:page_sql_con %>"></asp:SqlDataSource>
    <asp:DataGrid runat="server" ID="page_list" DataSourceID="page_select"></asp:DataGrid>
</asp:Content>
