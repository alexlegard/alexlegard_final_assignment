﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace final_project_pages
{
    public partial class Page : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Int32.Parse(Request.QueryString["pageid"]); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //debug.InnerHtml = "The page id you are looking for is " + pageid.ToString();

            //debug.InnerHtml = query;

            //want to write a function that gets the page information (author, title, content) from the result set 
            //of the database

            //get info for one row
            DataRowView pageinfo = getPageInfo(pageid);


            page_name.InnerHtml = pageinfo["pagetitle"].ToString();
            page_author.InnerHtml = pageinfo["pageauthor"].ToString();
            page_content.InnerHtml = pageinfo["pagecontent"].ToString();


        }
        protected DataRowView getPageInfo(int id)
        {
            string query = "select * from pages where pageid=" + pageid.ToString();

            page_select.SelectCommand = query;
            //debug.InnerHtml = query;
            //Another small manual manipulation to present the students name outside of
            //the datagrid.
            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

            //If there is no student in the studentview, an invalid id is passed
            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView classrowview = pageview[0]; //gets the first result which is always the page
            //string studentinfo = studentview[0][colname].ToString();
            return classrowview;

        }

        protected void deletePage(object sender, EventArgs e)
        {
            string basequery = "DELETE FROM Pages WHERE pageid =" + pageid.ToString();
            page_delete.DeleteCommand = basequery;
            page_delete.Delete();
            del_query.InnerHtml = basequery;

            //ALSO NEED TO DELETE ANY ROWS IN OTHER TABLES THAT REFERENCE THE ROW I JUST DELETED
            //But I don't think I need to since I only have one table...
        }
    }
}