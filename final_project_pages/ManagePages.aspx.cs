﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace final_project_pages
{
    public partial class ManagePages : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            pages_select.SelectCommand = "SELECT * FROM pages";

            //Call my function that I'll make in a bit
            page_list.DataSource = Manual_Bind(pages_select);

            //page_list.DataSource = pages_select;
            queryString.InnerHtml = "SELECT * FROM pages";
            page_list.DataBind();
        }
        /*WE WANT TO MAKE THE MANUAL BIND FUNCTION MAKE THE PAGE NAME APPEAR AS A LINK*/
        protected DataView Manual_Bind(SqlDataSource src)
        {
            //Get the data table and the data view
            DataTable tb;
            DataView view;

            //?????????????????????????????????????????????????
            tb = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            //LOOP THROUGH THE DATA SOURCE TO SWITCH THE PAGE NAMES

            //For each row...
            foreach (DataRow row in tb.Rows)
            {
                //...Get a column, and add a link to the pagetitle column.
                row["pagetitle"] = "<a href=\"Page.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["pagetitle"]
                    + "</a>";

                //<a href="Page.aspx?pageid=
                //1
                //">
                //Ninjas
                //</a>
            }
            //tb.Columns.Remove("pageid");
            view = tb.DefaultView;
            return view;
        }
    }
}