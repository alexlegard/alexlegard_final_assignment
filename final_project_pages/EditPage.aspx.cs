﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;

namespace final_project_pages
{
    public partial class EditPage : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        //Method that's just a little after page load
        protected override void OnPreRender(EventArgs e)
        {
            
            base.OnPreRender(e);
            //Get the row by putting the pageid in parentheses

            DataRowView pageRow = getPageInfo(pageid);

            //Validation
            if(pageRow == null)
            {
                debug.InnerHtml = "Page not found";
                return;
            }

            //Get an element from that row by putting the column name in parentheses
            pageName.Text = pageRow["pagetitle"].ToString();
            pageAuthor.Text = pageRow["pageauthor"].ToString();
            pageContent.Text = pageRow["pagecontent"].ToString();

        }

        protected void Edit_Page(object sender, EventArgs e)
        {
            string pname = pageName.Text;
            string pauthor = pageAuthor.Text;
            string pcontent = pageContent.Text;
            string pcategory = category_ddl.SelectedValue.ToString();

            string editquery = "UPDATE pages SET pagetitle ='"
                + pname
                + "', pageauthor='"
                + pauthor
                + "', pagecontent='"
                + pcontent
                + "', category='"
                + pcategory
                + "' WHERE pageid="
                + pageid;

            debug.InnerHtml = editquery;
            edit_page_src.UpdateCommand = editquery;
            edit_page_src.Update();
        }


        protected DataRowView getPageInfo(int id)
        {
            string query = "SELECT * FROM pages WHERE pageid=" + pageid.ToString();
            DataSource.SelectCommand = query;
            DataView pageview = (DataView)DataSource.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;
        }
    }
    
}