﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace final_project_pages
{
    public partial class AddNewPages : System.Web.UI.Page
    {
        protected void AddPage(object sender, EventArgs e)
        {
            /*When add page button is clicked, first we get the values of:
             * Page title(string)
             * Page author(string)
             * Page category(string)
             * Content(string)*/
            // Get title
            String title = pageName.Text.ToString();

            // Get author
            String author = pageAuthor.Text.ToString();

            // Get category
            String category = category_ddl.SelectedItem.ToString();

            // Get content
            String content = pageContent.Text.ToString();

            // Output to output div
            output.InnerHtml = title + author + content;

            // Write the title, author and content into the db as a query
            // INSERT INTO pages(pageid, pagetitle, pagecontent, pageauthor)
            // VALUES (pageid, pageName, pageAuthor, pageContent);
            
            String sqlString = "INSERT INTO pages(pagetitle, pagecontent, pageauthor, category) VALUES('"
                + title + "', '" + content + "', '" + author + "', '" + category +"');";
            DataSource.InsertCommand = sqlString;
            DataSource.Insert();
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}