﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Page.aspx.cs" Inherits="final_project_pages.Page" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Page</title>
    <link href="~/Style/Style.css" rel="stylesheet" type="text/css" media="screen" runat="server" />
</head>
<body>

    <div id="pages_links">
            <a id="edit_link" href="EditPage.aspx?pageid=<%Response.Write(this.pageid); %>"/>
            Edit Page
            </a>

            <a id="back_link" href="ManagePages.aspx"/>
            Back
            </a>
    </div>

    <form id="form1" runat="server">
        <div>
            <asp:SqlDataSource runat="server" ID="page_select"
                connectionString="<%$ ConnectionStrings:page_sql_con %>"></asp:SqlDataSource>
            <asp:SqlDataSource runat="server" ID="page_delete"
                connectionString="<%$ ConnectionStrings:page_sql_con %>"></asp:SqlDataSource>

            <h1 runat="server" id="page_name"></h1>
            <div runat="server" id="debug"></div>
            <div runat="server" id ="del_query"></div>
            
            <br><label>Author:</label>
            <span runat="server" id="page_author"></span>
            <p runat="server" id="page_content"></p>

            <%-->RUN AN SQL STATEMENT LIKE DELETE WHERE PAGEID=Int32.Parse(Request.QueryString["pageid"])--%>
            <asp:Button runat="server" ID="DeleteButton" Text="Delete Page" OnClick="deletePage"/>
            </asp:Button>
        </div>
    </form>
</body>
</html>