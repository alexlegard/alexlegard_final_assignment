﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="final_project_pages.EditPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit page</title>
    <link href="~/Style/Style.css" rel="stylesheet" type="text/css" media="screen" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <a id="back_link_editpage" href="ManagePages.aspx"/>Go Back</a>
            <h1>Edit Page</h1>
            <p>Page title:</p><!--Start with page title-->
            <asp:TextBox runat="server" ID="pageName" placeholder="name">
            </asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a name"
                ControlToValidate="pageName" ID="pageNameValidator">
            </asp:RequiredFieldValidator>
            <p>Page author:</p>
            <asp:TextBox runat="server" ID="pageAuthor" placeholder="author">
            </asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an author"
                ControlToValidate="pageAuthor" ID="pageAuthorValidator">
            </asp:RequiredFieldValidator>
            <p>Category:</p>
            <asp:DropDownList runat="server" ID="category_ddl">
                <asp:ListItem value="Platformer" text="Platformer"></asp:ListItem>
                <asp:ListItem Value="Shooter" Text="Shooter"></asp:ListItem>
                <asp:ListItem Value="Metroidvania" Text="Metroidvania"></asp:ListItem>
                <asp:ListItem Value="Horror" Text="Horror"></asp:ListItem>
                <asp:ListItem Value="RTS" Text="RTS"></asp:ListItem>
                <asp:ListItem Value="RPG" Text="RPG"></asp:ListItem>
                <asp:ListItem Value="ARPG" Text="ARPG"></asp:ListItem>
                <asp:ListItem Value="MMORPG" Text="MMORPG"></asp:ListItem>
                <asp:ListItem Value="TurnBasedStrategy" Text="Turn based strategy"></asp:ListItem>
                <asp:ListItem Value="Fighting" Text="Fighting"></asp:ListItem>
                <asp:ListItem Value="Survival" Text="Survival"></asp:ListItem>
                <asp:ListItem Value="Sandbox" Text="Sandbox"></asp:ListItem>
                <asp:ListItem Value="InteractiveNovel" Text="Interactive novel"></asp:ListItem>
                <asp:ListItem Value="CCG" Text="CCG"></asp:ListItem>
                <asp:ListItem Value="Action" Text="Action"></asp:ListItem>
                <asp:ListItem Value="Adventure" Text="Adventure"></asp:ListItem>
                <asp:ListItem Value="Puzzle" Text="Puzzle"></asp:ListItem>
                <asp:ListItem Value="Dungeon" Text="Dungeon crawler"></asp:ListItem>
                <asp:ListItem Value="Roguelike" Text="Roguelike"></asp:ListItem>
                <asp:ListItem Value="MOBA" Text="MOBA"></asp:ListItem>
                <asp:ListItem Value="Sport" Text="Sport"></asp:ListItem>
            </asp:DropDownList>
            <p>Content</p>
            <asp:TextBox runat="server" ID="pageContent" placeholder="content">
            </asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter some page content"
                ControlToValidate="pageContent" ID="ContentValidator">
            </asp:RequiredFieldValidator>
            <br><asp:Button runat="server" ID="theButton" Text="Submit" OnClick="Edit_Page"/>
            <div id="output" runat="server"></div>
            <!--AND THEN MAKE A QUERY-->
            <asp:SqlDataSource runat="server" ID="DataSource" ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
            </asp:SqlDataSource>
            <asp:SqlDataSource runat="server" ID="edit_page_src" ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
            </asp:SqlDataSource>
            <div id="debug" runat="server"></div>
        </div>
    </form>
</body>
</html>
